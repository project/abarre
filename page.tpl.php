<?php 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
	<!--[if lte IE 6]>
	  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/ie6.css";</style>
	<![endif]-->
	<!--[if IE 7]>
  	<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/ie7.css";</style>
	<![endif]-->

</head>

<body class="<?php print $body_classes; ?>">

    <div id="header">

        <?php print $search_box; ?>      
        <?php if ($logo): ?>
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
          </a>
        <?php endif; ?>

    <div id="name-and-slogan">
        
        <?php if ($site_name): ?>
          <h1 id='site-name'>
            <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
          </h1>
        <?php endif; ?>
        
        <?php if ($site_slogan): ?>
          <div id='site-slogan'>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>
        
    </div> <!-- /name-and-slogan -->

    </div> <!-- /header -->

    <div id="content" class="clear-block">
                  
      	<div id="main" class="column main-content">
      	  <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
      	  <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
      	  <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
      	  <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      	  <?php print $help; ?>
      	  <?php print $messages; ?>
      	  <?php print $content; ?>
      	  <?php print $feed_icons; ?>
      	  <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>

      	</div> <!-- /main -->

        <div id="sidebar-left" class="column sidebar">
	
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>

				<?php if ($secondary_links): ?>
					<div id="secondary" class="block">
						<div class="content">
           		<?php print theme('menu_links', $secondary_links); ?>
						</div>
					</div>
        <?php endif; ?>

          <?php print $sidebar_left; ?>

        </div> <!-- /sidebar-left -->

    </div> <!-- /content -->

      <div id="footer">
				<div id="corner"></div>
				<div class="content">
        	<?php print $footer_message; ?>
 				</div>
     </div> <!-- /footer -->
    
    <?php print $closure; ?>
    
</body>
</html>