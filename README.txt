
Abarre, By CouzinHub // couzin.hub@gmail.com

/////// INSTALLATION  /////////

  1. Download Abarre from http://drupal.org/project/abarre

  2. Unpack the downloaded file and place the abarre folder in your Drupal installation under one of the following locations:

       sites/all/themes : making it available to all Drupal sites in a mult-site configuration

       sites/default/themes : making it available to only the default Drupal site

       sites/example.com/themes : making it available to only the example.com site if there is a sites/example.com/settings.php configuration file

  3. Log in as an administrator on your Drupal site and go to :

	Administer > Site building > Themes (admin/build/themes) 
     
	and make abarre the default theme.

  4. From the Theme settings page (admin/build/themes) configure the Abarre theme.